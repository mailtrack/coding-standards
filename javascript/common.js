module.exports = {
    "extends": [
        "eslint:recommended",
        "plugin:jest/recommended"
    ],
    "parserOptions": {
        "ecmaVersion": 2021,
        "sourceType": "module"
    },
    "env": {
        "es2021": true,
        "node": true,
        "jest": true
    },
    "plugins": [
        "no-only-tests",
        "import",
        "jest"
    ],
    "rules": {
        "indent": ["error", 4, {
            "ignoredNodes": ["TemplateLiteral > *"]
        }],
        "semi": ["error", "always"],
        "no-console": "error",
        "function-call-argument-newline":["error", "consistent"],
        "no-unmodified-loop-condition": 1,
        "quotes": ["error", "single"],
        "sort-imports": "warn",
        "space-before-function-paren": [
            "error",
            {
                "anonymous": "never",
                "named": "never",
                "asyncArrow": "always"
            }
        ],
        "object-curly-spacing": ["error", "always"],
        "no-async-promise-executor": "off",
        "no-only-tests/no-only-tests": "error",
        "import/no-default-export": "warn",
        "no-promise-executor-return": "error"
    }
};
