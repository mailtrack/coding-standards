# Javascript Coding Standard

We use eslint to find problems and follow a coding standard in our JS files.

Our CS is based in eslint recommended rules. You can check out these rules here: https://eslint.org/docs/rules/

We do not follow the Airbnb JS Style Guide, but it is a good reference to read, and has lots of rules in common
with eslint recommended rules. Check out the guide here: https://github.com/airbnb/javascript

## How to config CS in a new project

* Install required node modules: `npm install --save-dev eslint` 
* Add this line to the devDependencies property of the package.json file:
   ```
   "eslint-config-mt-cs": "https://git@bitbucket.org/mailtrack/coding-standards.git",
   ```
* run `npm install`
* create a file called `.eslintrc` in the project's root with this content:
    ```json
    {
        "extends": [
            "mt-cs/javascript"
        ]
    }
    ```
  
## How to use a specific branch or tag of the CS

By default, NPM will install the master branch of this project. If you need to use another branch or tag append `#branch_name` or `#tag_name` to the git url in the package.json. For example:
```
"eslint-config-mt-cs": "https://git@bitbucket.org/mailtrack/coding-standards.git#1.0.0",
```

## How to test changes in local

If you make some changes, and you want to test them before pushing the code, you can change the dependency url to the local path. Por example:
```
"eslint-config-mt-cs": "../coding-standards"
```
Also, you may need to temporary install the needed ESLint plugins into the project where you are going to test the changes in order to let ESLint resolve them.

## Configure ESLint in your IDE

* PHP Storm: https://www.jetbrains.com/help/phpstorm/eslint.html#ws_js_eslint_activate
* VS Code: https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
