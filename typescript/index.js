module.exports = {
    "extends": [
        "../javascript/common",
        "plugin:@typescript-eslint/recommended",
    ],
    "parser": "@typescript-eslint/parser",
    "plugins": [
        "@typescript-eslint"
    ],
    "rules": {
        "@typescript-eslint/no-explicit-any": "warn",
        "@typescript-eslint/no-empty-function": "warn",
        "@typescript-eslint/ban-ts-comment": "warn",
        "@typescript-eslint/no-unnecessary-type-assertion": "off",
        "@typescript-eslint/no-floating-promises": "off",
        "@typescript-eslint/no-misused-promises": "off",
        "@typescript-eslint/no-implicit-any-catch": "off"
    },
    "overrides": [{
        "files": ["**/webpack/**/*.js", "**/webpack.*"],
        "rules": {
            "@typescript-eslint/no-var-requires": "off"
        }
    }]
}

