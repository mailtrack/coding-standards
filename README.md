# Mailtrack Coding Standards

This project contains coding standards documentation and config files for our projects

* [Javascript](./javascript)
* [Typescript](./typescript)
* [SCSS](./scss)

## Semantic Versioning

This project is semantic versioned, please create a new version tag if you make changes.

## Common Rules
### Development Language
English is our main development language. That means that everything in the code
should be written in English: classes, entities, functions, methods, etc. If there
is a new object needed, think about the best english term to use and check it with some mate.

It does not matter if Business handles terms in Spanish, we should be capable of understanding
and translating it into English and viceversa.

### Method Names Conventions
If the method returns a boolean value, use *is* or *has* as the prefix for the
method name. For example, use `isCanceled()` for methods that return true or false values.

Avoid the use of the word *not* in the boolean method name, use the "!" operator instead.

    $purchase->isNotCanceled() -> !$purchase->isCanceled()

When an entity has a relation with other entities, use `get|has|remove|add|countOtherEntity(s)()` and
`setOtherEntities()` form.

Prefer meaningful names, ie use `changeName` instead of `setName` . 

Avoid anemic domain model:
 - Do not add setters and getters to an object if they are not strictly necessary. 
 - Favour 'Tell don't ask principle' to encapsulate business logic inside entities.

Make al methods private by default, use protected only if it is strictly necessary.

### Common Interfaces method names
#### Repositories
When we have to define a Repository interface the method names are normalized:

 * find(): Find by the primary key (whatever it is), pass an array if multiple pk
 * findOrThrow(): Find by the primary key (whatever it is), pass an array if multiple pk and throw exception (for example UserNotFoundException) in case it doesn't find it.
 * findOneBy()
 * findAll()
 * findBy() | findByXXX()
 * findByOrThrow() | findByXXXOrThrow()
 * persist(): Insert or update (automatically based if entity has pk defined)
 * insert()
 * update()
 * remove()
 * Others: take a look how Doctrine does it

Methods that should return one element, when existing will return it, when not, will return `null`. For
methods that should return more than one element, when existing will return an array of elements, when
not, will return `[]`.

Regarding the specific implementation for **Doctrine**, we agreed on using `QueryBuilder` over `SQL`.

### SQL

When we add/modify an SQL statement (even Updates) we should put comment in the PR comparing both explains (the old query and the new query), or if it's a new query, only the new explain to check that it won't affect the current performance.

We must remember that every time we fetch a relationship from the entity it is transformed into a query and could probably be improved.

For example:
`$mail->getOpens();` This will load all opens with a not optimized query, and probably we don't need all.

Important:
We agreed on using `QueryBuilder` over `SQL`.
Avoid using:
~~~
    $em->createQuery('SELECT u FROM User u WHERE u.id = :id')
        ->setParameter('id', $id)
        ->getSingleResult();
~~~
use instead:
~~~
    $em->createQueryBuilder()
        ->select('u')
        ->from('User', 'u')
        ->where('u.id = :id')
        ->setParameter('id', $id)
        ->getQuery()
        ->getSingleResult();
~~~

### Extract till you drop
Related to other development style techniques, we should follow as much as we can
[Clean Code Book](http://www.amazon.es/Robert-Martin-Clean-Collection-Series-ebook/dp/B00666M59G)
recommendations related to functions, parameters, null values, etc. The most important concept is "Extract till
you drop" really related to refactoring concepts and how we document our code.

* [SymfonyLive London 2013 - Mathias Verraes - Extract till you drop](http://www.youtube.com/watch?v=DCvDF0lbCbc)
* [SymfonyLive London 2013 - Mathias Verraes - Extract till you drop - Webcast](http://www.youtube.com/watch?v=S9pKJxOPmWM)
* [Testing and Refactoring Legacy Code](http://www.youtube.com/watch?v=_NnElPO5BU0)

### Comments
Best naming over inline comments is our policy. Check "Extract till you drop" concept in this page. We prefer
a right name expressing intention for a function, class, service, etc. than adding a comment that explains whatever.

### Boy Scout Rule

Leave the code better than you found it. It is not always possible to take the approach of shutting down all 
value-adding work and simply trying to clean up the codebase. The Boy Scout Rule suggests an alternative approach, which is to simply try to ensure that with each commit, you leave the code better than you found it.  Maybe only slightly. Following this principle, teams can improve the quality of their code over time, while continuing to deliver value to their customers and stakeholders.

### Types
We use type hinting when possible (method and return types). Favour type hinting over code comments.

### Testing

All the code for testing purposes will be under test directory. This includes the InMemoryRepository that we only use for testing purposes.
The exception to this rule is a complete repository without incomplete methods.

All the tests should include `setUp()` method to initialize the object to test.

We will use `InMemoryRepository` over `Mock`, for things like `UserRepository` or `MailRepository` that are repeated in many tests.

### Configuration YAML

We stop duplicating Infrastructure services with a Domain alias. We will use the Infrastructure Services directly.

New services would be declared directly like:
~~~
services:
    Mailtrack\Domain\MyService:
        arguments: ['@doctrine.orm.entity_manager']
~~~
instead of:
~~~
services:
    mailtrack.domain.my_service:
        class: Mailtrack\Domain\MyService
        arguments: ['@doctrine.orm.entity_manager']
~~~

### Use of Feature Rules over User Tier

Not all of our features are available or behave the same for all of our users. Some of them are only available to our paying users, for example.

Feature availability can change over time and we typically control this with Feature Rules. This gives us a lot of flexibility without having to make big code changes.

For this to work well, whenever we have to make conditionals in the code based on the availability of a feature, it is preferable to do it based on a Feature Rule, instead of looking at the user's tier (Free, Pro, ...).

## Related resources

* [Extension architecture](https://bitbucket.org/mailtrack/extension/src/master/app/docs/architecture.md)
* [Backend Coding Standard (it has some common rules with this doc)](https://bitbucket.org/mailtrack/backend/src/master/docs/tech/coding-standard.md)